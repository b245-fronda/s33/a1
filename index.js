// #3 create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos", {method: "GET"})

// converting to json
.then(response => response.json())

// #4 using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console
.then(data => {
	let title = data.map(element => element.title);
console.log(title);

});

// #5 create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "GET"})

// converting to json
.then(response => response.json())

// #6 using the data retrieved, print a message in the console that will provide the title and status of the to do list item
.then(data => console.log(`The item "${data.title}" on the list has a status of ${data.completed}`

));

// #7 create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));

// #8 create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({

		// #9 update a to do list item by changing the data structure to contain the following properties:
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));

// #10 create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({

		// #11 update a to do list item by changing the status to complete and add a date when the status was changed
		dateCompleted: "07/09/21",
		description: "To update the my to do list with a different data structure",
		status: "Complete",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(result => console.log(result));

// #12 create a fetch request using the delete method that will delete an item using the JSON placeholder API
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"})
.then(response => response.json())
.then(result => console.log(result));